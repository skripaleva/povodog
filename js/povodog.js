$(document).ready(function()
{
	$("#popup span").on("click", function()
	{
		$("#popup, #layout").fadeOut(700);
	});

	var width = $(window).width();
	if(width<1416) $("#pets-right").hide();

	$(".radio").each(function(){changeRadioStart($(this));});
	$(".cb").each(function(){changeCheckStart($(this));});
	$(".cb").mousedown(function(){changeCheck($(this));});

	if($("select").is("#age")) cuSel({changedEl:"select", visRows:20});

	$("#nav a").live("click",function()
	{
		filter($(this).attr("href"));
		return false;
	});

	$("#filter input[type=button]").click(function()
	{
		filter(1);
		return false;
	});

	$("#preview ul li a").click(function()
	{
		$("#preview ul li.active").removeClass("active");
		$(this).parent("li").addClass("active").parent("ul").parent("div").children("img").attr("src", $(this).attr("href"));
		return false;
	});

	$("#iwantdog span, #overlay").click(function(){$("#overlay, #iwantdog").fadeOut(900);});
	$("#pet input").click(function(){$("#overlay, #iwantdog").fadeIn(900);});

	$("#iwd").validate(
	{
		submitHandler:function(form){$.get("/iwd.php", $(form).serialize(), function(){$(form).trigger('reset'); $.jGrowl('������� �� ������! ��������� ������ � ��������� ����� �������� � ����.',{position:'center'}); $("#overlay, #iwantdog").fadeOut(900);});}
	});

	$(".warnings").hide();

	if($("div").is("#marquee")) $("#marquee").marquee({duration:12000, pauseOnHover:true});
	// console.log($('.content_form'));
    if ($('.content_form').find($('.box__body')).length >= 1) {
        $.magnificPopup.open({
            items: {
                src: '#donation',
                type: 'inline'
            },
            closeBtnInside: true
        });
        $('.popup-with-form').magnificPopup({
            type: 'inline'
        });
    } else {
        $('.popup-with-form').magnificPopup({
            type: 'inline'
        });
    }
    $('.js-example-basic-hide-search').select2({
        minimumResultsForSearch: Infinity
    });

    formHandler();

});
function formHandler() {
    $(document).on('submit', '.js-donation-form', function (e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            type: 'POST',
            url: "/ajax/donate-sent.php",
            data: form.serialize(),
            success: function($response) {
                if( $response.indexOf('redirect:') !== -1 ){
                    //�� ������
                    response = $response.split('redirect:');
                    window.location = response[1];
                }/* else {
                    $('.content_form').html('�����');
                }*/
            }
        });
    });
}
function filter(page)
{
	var f = $("#filter").serialize()+"&page="+page;
	$("#pets").load("/filter.php", f);
}

function changeRadio(el)
{
	var el = el,
	input = el.find("input").eq(0);
	var nm=input.attr("name");
	jQuery(".radio input").each(
	function() {
		if(jQuery(this).attr("name")==nm)
		{
			jQuery(this).parent().removeClass("radioChecked");
		}
	});

	if(el.attr("class").indexOf("radioDisabled")==-1)
	{	
		el.addClass("radioChecked");
		input.attr("checked", true);
	}
    return true;
}

function changeVisualRadio(input)
{
	var wrapInput = input.parent();
	var nm=input.attr("name");

	$(".radio input").each(

	function() {
		if(jQuery(this).attr("name")==nm)
		{
			jQuery(this).parent().removeClass("radioChecked");
		}
	});

	if(input.attr("checked"))
	{
		wrapInput.addClass("radioChecked");
	}
}

function changeRadioStart(el)
{
try
{
var el = el,
	radioName = el.attr("name"),
	radioId = el.attr("id"),
	radioChecked = el.attr("checked"),
	radioDisabled = el.attr("disabled"),
	radioTab = el.attr("tabindex");
	radioValue = el.attr("value");
	if(radioChecked)
		el.after("<span class='radio radioChecked'>"+
			"<input type='radio'"+
			"name='"+radioName+"'"+
			"id='"+radioId+"'"+
			"checked='"+radioChecked+"'"+
			"tabindex='"+radioTab+"'"+
            "value='"+radioValue+"' /></span>");
	else
		el.after("<span class='radio'>"+
			"<input type='radio'"+
			"name='"+radioName+"'"+
			"id='"+radioId+"'"+
			"tabindex='"+radioTab+"'"+
	        "value='"+radioValue+"' /></span>");

	if(radioDisabled)
	{
		el.next().addClass("radioDisabled");
		el.next().find("input").eq(0).attr("disabled","disabled");
	}

	el.next().bind("mousedown", function(e) { changeRadio(jQuery(this)) });
	el.next().find("input").eq(0).bind("change", function(e) { changeVisualRadio(jQuery(this)) });
	if(jQuery.browser.msie)
	{
		el.next().find("input").eq(0).bind("click", function(e) { changeVisualRadio(jQuery(this)) });	
	}
	el.remove();
}
catch(e)
{
	// ���� ������, ������ �� ������
}
    return true;
}

function changeCheck(el)
{
	var el = el,
	input = el.find("input").eq(0);
	if(!input.attr("checked"))
	{
		el.css("background-position","0 -14px");
		input.attr("checked", true)
	}
	else
	{
		el.css("background-position","0 0");
		input.attr("checked", false)
	}
	return true;
}

function changeCheckStart(el)
{
	var el = el,
	input = el.find("input").eq(0);
	if(input.attr("checked")){el.css("background-position","0 -15px");}
	return true;
}

$(window).resize(function(){
    var width = $(window).width();
    if(width<1416) $("#pets-right").fadeOut();
    else $("#pets-right").fadeIn();
});

$(document).ready(function() {
// Type Image Zoom - �������� � ���������
    $('.image-popup-zoom').magnificPopup({
        type: 'image',
        zoom: {
            enabled: true,
            duration: 300 // ����������������� ��������. �� ������� ������ �������� ����� � � CSS
        }
    });
});