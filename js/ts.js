$(document).ready(function()
{
	$("#popup span").on("click", function()
	{
		$("#popup, #layout").fadeOut(700);
	});

if($("div").is("#marquee")) $("#marquee").marquee({duration:12000, pauseOnHover:true});

$("#prev").click(function(){navigate("back");});
$("#next").click(function(){navigate("next");});

var photos = [];
var slideshowSpeed = 7000;
var count = 0;

$('#teaser img').each(function(index){
	photos[index] = {image:$(this).attr('src'), code:$(this).attr('data-code'), name:$(this).attr('data-name'), text:$(this).attr('data-text')};
});

var interval;
var activeContainer = 1;
var currentImg = 0;
var animating = false;
var navigate = function(direction) {
    if(animating) return;

    if(direction == "next") {
        currentImg++;
        if(currentImg == photos.length + 1) {
            currentImg = 1;
        }
    } else {
        currentImg--;
        if(currentImg == 0) {
            currentImg = photos.length;
        }
    }

    var currentContainer = activeContainer;
    if(activeContainer == 1) {
        activeContainer = 2;
    } else {
        activeContainer = 1;
    }
    showImage(photos[currentImg - 1], currentContainer, activeContainer);
};

var currentZindex = -1;
var showImage = function(photos, currentContainer, activeContainer) {
    animating = true;

    currentZindex--;

    $("#ts" + activeContainer).css({
		"background-image" : "url("+photos.image+")",
		"display" : "block",
		"z-index" : currentZindex
    });

	$("#dog div").fadeOut(100,function()
	{
		$("#dog div").html('<a href="/pets/'+photos.code+'/">'+photos.name+'</a><em>'+photos.text+'</em>').fadeIn(100);
		$("#iwannadog").attr("href","/pets/"+photos.code+"/");
		$("#share .vk").attr("href","http://vk.com/share.php?url=http://povodog.com/pets/"+photos.code+"/");
		$("#share .fb").attr("href","https://www.facebook.com/sharer/sharer.php?src=sp&u=http://povodog.com/pets/"+photos.code+"/");
	});

    $("#ts" + currentContainer).fadeOut(2000,function(){
        setTimeout(function(){animating = false;}, 1);
    });
};

var stopAnimation = function() {
    clearInterval(interval);
};

navigate("next");

interval = setInterval(function() {
	navigate("next");
}, slideshowSpeed);

});